﻿using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public interface IInitializerDb
    {
        Task InitializeAsync();
    }
}