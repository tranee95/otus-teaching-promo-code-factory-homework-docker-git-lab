﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Options
{
    public class ConnectionString
    {
        public string SqlLite { get; set; }
    }
}