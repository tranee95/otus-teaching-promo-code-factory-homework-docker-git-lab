﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Options
{
    public class ConfigurationOptions
    {
        public ConnectionString ConnectionStrings { get; set; }
    }
}