﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Models
{
    public class PromoCode : BaseEntity
    {
        [MaxLength(70)]
        public string Code { get; set; }
        
        [MaxLength(150)]
        public string ServiceInfo { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PartnerName { get; set; }
        public virtual Employee PartnerManager { get; set; }
        public virtual Preference Preference { get; set; }
    }
}